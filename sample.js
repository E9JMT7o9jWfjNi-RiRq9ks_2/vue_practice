Vue.createApp({
  data: function () {
    return {
      count: 0,
    }
  },
  methods: {
    onClickCountUp: function (e) {
      console.log(e);
      this.count += 1
    },
  },
}).mount('#app')