Vue.createApp({
  data: function () {
    return {
      todoTitle: '',
      todoDescription: '',
      todoCategories: [],
      selectedCategory: '',
      todos: [],
      categories: [],
      hideDoneTodo: false,
      searchWord: '',
      order: 'desc',
      categoryName: '',
    }
  },

  // 算出プロパティ：computed
  // dataで指定した2つ以上の値をつなげて表示したい場合や、
  // ユーザーから入力された値に任意の文字列が入っているのかを
  // 判定する場合など、管理しているデータを加工したり検証したりできる機能
  computed: {
    canCreateTodo: function () {
      return this.todoTitle !== ''
    },
    canCreateCategory: function () {
      return this.categoryName !== '' && !this.existsCategory
    },
    existsCategory: function () {
      const categoryName = this.categoryName

      return this.categories.indexOf(categoryName) !== -1
    },
  },

  // methodsプロパティ
  // dataの変更と同時に他の処理をする場合に使用する
  methods: {
    createTodo: function () {
      if (!this.canCreateTodo) {
        return
      }

      this.todos.push({
        id: 'todo-' + Date.now(),
        title: this.todoTitle,
        description: this.todoDescription,
        categories: this.todoCategories,
        dateTime: Date.now(),
        done: false,
      })

      // ToDoタスクを追加する処理
      this.todoTitle = ''
      this.todoDescription = ''
      this.todoCategories = []
    },

    createCategory: function () {
      if (!this.canCreateCategory) {
        return
      }
      // カテゴリを追加する処理
      this.categories.push(this.categoryName)
      this.categoryName = ''
    },
  },
}).mount('#app'